#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
tf_pub.py:

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
from math import *
import numpy as np

#import tf
#from tf.transformations import euler_matrix, quaternion_from_euler, euler_from_matrix, euler_from_quaternion, identity_matrix
import tf2_ros


###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import NavSatFix
from mavros_msgs.msg import State

import geometry_msgs.msg

###############################################
# ROS Service messages                        #
###############################################
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse

###############################################
# Offboad Control class                       #
###############################################
class TFPub:
    def __init__(self, *args):
        rospy.init_node('tf_publisher')
        self.current_local_pose = PoseStamped()

        #self.br = tf.TransformBroadcaster()
        self.br = tf2_ros.TransformBroadcaster()

        # Subscriber
        self.pose_sub = rospy.Subscriber('/mavros/local_position/pose', PoseStamped, self.cb_local_pose, queue_size = 1)

        rospy.spin()

    def cb_local_pose(self,data):
        self.current_local_pose = data
        #print(self.current_local_pose)

        self.send_tf(self.current_local_pose)


    def send_tf(self, pose):
        #self.br = tf2_ros.TransformBroadcaster()
        t = geometry_msgs.msg.TransformStamped()

        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "map"

        t.child_frame_id = "sensor_link"

        t.transform.translation.x = pose.pose.position.x
        t.transform.translation.y = pose.pose.position.y
        t.transform.translation.z = pose.pose.position.z

        t.transform.rotation.x = pose.pose.orientation.x
        t.transform.rotation.y = pose.pose.orientation.y
        t.transform.rotation.z = pose.pose.orientation.z
        t.transform.rotation.w = pose.pose.orientation.w

        self.br.sendTransform(t)


if __name__ == '__main__':
    TFP = TFPub()
